<?php

/**
 * @file
 * Facet API hook implementations.
 */

/**
 * Implements hook_facetapi_widgets().
 */
function facetapi_select_autocomplete_facetapi_widgets() {
  return array(
    'select_autocomplete' => array(
      'handler' => array(
        'label' => t('Select Autocomplete'),
        'class' => 'FacetapiWidgetSelectAutocomplete',
        'query types' => array('term'),
      ),
    ),
  );
}

