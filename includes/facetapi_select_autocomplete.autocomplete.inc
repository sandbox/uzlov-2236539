<?php

/**
 * @file
 * Contains select autocomplete page callbacks.
 */

/**
 * Page callback for getting select autocomplete results.
 */
function facetapi_select_autocomplete_callback_json($index_name, $facet_name, $current_window_id, $search = '') {
  $ret = array();

  $_SESSION['facetapi_select_autocomplete']['current_window_id'] = $current_window_id;

  $current_searcher = 'search_api@' . $index_name;
  $adapter = facetapi_adapter_load($current_searcher);
  if (!empty($adapter) && $current_window_id != '0') {
    // Load facet.
    $facet_info = facetapi_facet_load($facet_name, $current_searcher);
    $facet_info['allowed_operators']['or'] = 0;
    $facet = $adapter->getFacet($facet_info);
    $realm = facetapi_realm_load('block');
    // Load facet settings.
    $facet_settings = $adapter->getFacetSettings($facet_info, $realm);

    // Load previous search from user session.
    $params = array();
    if (isset($_SESSION['facetapi_select_autocomplete']['all_windows'][$current_window_id]['curent_search_query'])) {
      $curent_search_query = $_SESSION['facetapi_select_autocomplete']['all_windows'][$current_window_id]['curent_search_query'];
      $params = $curent_search_query;
    }
    if (!empty($params)) {
      //check and unset if need, already exist current facet in search
      foreach ($params['f'] as $key => $param) {
        $key_facet_name = explode(':', $param);
        $key_facet_name = reset($key_facet_name);
        //unset from params the same query
        if ($key_facet_name == $facet_name) {
          unset($params['f'][$key]);
        }
      }
      $params['f'][] = $facet_name . ':' . $search;
    }
    else {
      $params = array(
        'f' => array($facet_name . ':' . $search),
      );
    }
    $adapter->setParams($params);

    // Set reset query to FALSE before execute.
    $_SESSION['facetapi_select_autocomplete']['all_windows'][$current_window_id]['reset_search_query'] = FALSE;

    $query = search_api_query($index_name);
    $query->range(0, $facet_settings->settings['number_of_results']);
    $result = $query->execute();
    // generate facet build
    $adapter->processFacets();
    $processor = new FacetapiFacetProcessor($facet);
    $processor->process();
    $facet_build = $processor->getBuild();

    //unset values from build with count 0
    foreach ($facet_build as $value_key => &$value) {
      if (!$value['#count']) {
        unset($facet_build[$value_key]);
      }
    }
    $facet_build = array_slice($facet_build, 0, $facet_settings->settings['number_of_results'], TRUE);

    foreach ($facet_build as $value_key => $value) {
      $ret[$value['#markup']] = $value['#markup'];
    }

    drupal_json_output($ret);
  }
  else {
    return t('Please, use AJAX!');
  }
}
