<?php

/**
 * Render function for facetapi_select_autocomplete_facet_form().
 */
function facetapi_select_autocomplete_facet_form($form, &$form_state, $settings, $options, $count = 0, $facet, $pretty_paths_alias) {
  // unique window id for each full page reload
  $window_id = 0;
  if (isset($_SESSION['facetapi_select_autocomplete']['current_window_id'])) {
    $window_id = $_SESSION['facetapi_select_autocomplete']['current_window_id'];
  }
  $form_state['facetapi_select_autocomplete']['window_id'] = $window_id;

  $facet_info = $facet->getFacet();
  $adapter = $facet->getAdapter();
  $current_searcher = $adapter->getSearcher();
  $index_name = explode('@', $current_searcher);
  $index_name = $index_name[1];

  // Check adapter url processor.
  // Take curent search query settings only for pretty_paths(or standart) url processor.
  $searchers_info = facetapi_get_searcher_info();
  $adapter_info = $searchers_info[$current_searcher];
  $form_state['facetapi_select_autocomplete']['url processor'] = $adapter_info['url processor'];

  foreach ($options as $key => $option) {
    if ($option['#active']) {
      $current_active_item = $option['#markup'];
    }
  }

  $form['#id'] = 'facetapi_select_autocomplete_facet_form_' . $count;
  $form['facet_autocomplete'] = array(
    '#type' => 'textfield',
    '#default_value' => !empty($current_active_item) ? $current_active_item : '',
    '#size' => 'auto',
    '#id' => 'facetapi_select_autocomplete_input_' . $count,
    '#attributes' => array(
      'placeholder' => t($settings->settings['default_autocomplete_placeholder']),
    ),
    '#autocomplete_path' => 'facetapi_select_autocomplete/' . $index_name . '/' . $facet_info['name'] . '/' . $window_id,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#attributes' => array('class' => array('facetapi-select-autocomplete-submit')),
//    '#attributes' => array('style' => array('display:none;')),
    '#value' => t('Submit'),
  );
  return $form;
}

/**
 * Submit function for facetapi_select_autocomplete_facet_form().
 */
function facetapi_select_autocomplete_facet_form_submit($form, &$form_state) {
  // unset not used unique window id from user session
  if (isset($_SESSION['facetapi_select_autocomplete']['all_windows'][$form_state['facetapi_select_autocomplete']['window_id']])) {
    unset($_SESSION['facetapi_select_autocomplete']['all_windows'][$form_state['facetapi_select_autocomplete']['window_id']]);
  }

  $settings = $form_state['build_info']['args'][0];
  $options = $form_state['build_info']['args'][1];
  $pretty_paths_alias = $form_state['build_info']['args'][4];

  $facet_value = trim($form_state['values']['facet_autocomplete']);
  // if empty value - use default value for remove filters
  if (empty($facet_value)) {
    $facet_value = $form['facet_autocomplete']['#default_value'];
  }
  foreach ($options as $key => $option) {
    if ($facet_value == $option['#markup']) {
      $path = $option['#path'];
      if (module_exists('facetapi_pretty_paths') && $form_state['facetapi_select_autocomplete']['url processor'] == 'pretty_paths') {
        //clear all part of path from this facet but use last
        $path_array = explode('/', $path);
        $path_date_keys = array_keys($path_array, $pretty_paths_alias);
        if (count($path_date_keys) > 1) {
          unset($path_array[$path_date_keys[0]]);
          unset($path_array[$path_date_keys[0] + 1]);
        }
        $path = implode('/', $path_array);
        $option['#path'] = $path;
      }
      else {
        //clear all query from this facet but use last
        if (!empty($option['#query'])) {
          foreach ($option['#query']['f'] as $query_key => $query_part) {
            $query = explode(':', $query_part);
            if ($query[0] == $settings->facet && $query[1] != $facet_value) {
              unset($option['#query']['f'][$query_key]);
            }
          }
          $option['#query']['f'] = array_values($option['#query']['f']);
        }
      }
      $form_state['redirect'] = array($option['#path'], array('query' => $option['#query']));
      break;
    }
  }
}

/**
 * Widget that renders facets as dropdowns.
 */
class FacetapiWidgetSelectAutocomplete extends FacetapiWidgetLinks {

  /**
   * Get all childrenss.
   */
  public function get_all_childrens($element) {
    $options = array();
    foreach ($element as $key => $option) {
      if (isset($option['#item_children'])) {
        $options = $options + $this->get_all_childrens($option['#item_children']);
        $option['#item_children'] = array();
        $options[] = $option;
      }
    }
    return $options;
  }

  /**
   * Renders the links.
   */
  public function execute() {
    static $count = 0;
    $count++;
    $settings = $this->settings;

    $element = &$this->build[$this->facet['field alias']];
    $options = $this->get_all_childrens($element);

    // integration with facetapi_pretty_paths
    $pretty_paths_alias = '';
    if (module_exists('facetapi_pretty_paths')) {
      // Get global facet settings.
      $adapter = $this->facet->getAdapter();
      $global_facet_settings = $adapter->getFacetSettingsGlobal($this->build['#facet']);
      // Get pretty paths alias from global facet settings.
      $pretty_paths_alias = !empty($global_facet_settings->settings['pretty_paths_alias']) ? $global_facet_settings->settings['pretty_paths_alias'] : $this->build['#facet']['field alias'];
    }
    // We keep track of how many facets we're adding,
    // because each facet form needs a different form id.
    $element = drupal_get_form('facetapi_select_autocomplete_facet_form', $settings, $options, $count, $this->facet, $pretty_paths_alias);
  }

  public function settingsForm(&$form, &$form_state) {
    parent::settingsForm($form, $form_state);
    unset($form['widget']['widget_settings']['links'][$this->id]['soft_limit']);

    $form['widget']['widget_settings']['select_autocomplete'][$this->id]['default_autocomplete_placeholder'] = array(
      '#title' => t('Default placeholder'),
      '#type' => 'textfield',
      '#default_value' => isset($this->settings->settings['default_autocomplete_placeholder']) ? $this->settings->settings['default_autocomplete_placeholder'] : '',
      '#states' => array(
        'visible' => array(
          'select[name="widget"]' => array('value' => $this->id),
        ),
      ),
    );
    $form['widget']['widget_settings']['select_autocomplete'][$this->id]['number_of_results'] = array(
      '#title' => t('Number of results'),
      '#type' => 'select',
      '#options' => drupal_map_assoc(array(3, 5, 10, 15, 20, 30, 40, 50, 75, 100)),
      '#default_value' => isset($this->settings->settings['number_of_results']) ? $this->settings->settings['number_of_results'] : 10,
      '#states' => array(
        'visible' => array(
          'select[name="widget"]' => array('value' => $this->id),
        ),
      ),
    );
  }

  /**
   * Overrides FacetapiWidget::getDefaultSettings().
   */
  function getDefaultSettings() {
    return array(
      'default_autocomplete_placeholder' => t('Select'),
      'number_of_results' => 10,
      'nofollow' => 1,
      'show_expanded' => 0,
    );
  }

}
